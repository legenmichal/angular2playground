export class User {
	constructor(id: number, firstName: string, lastName: string, city: string){
		this.Id = id;
		this.FirstName = firstName;
		this.LastName = lastName;
		this.City = city;
	}
	
	Id: number;
	FirstName: string;
	LastName: string;
	City: string;	
	
	public static Clone(user: User) : User
	{
		return new User(user.Id, user.FirstName, user.LastName, user.City);
	}	
	
	public Copy(user: User)
	{
		this.Id = user.Id;
		this.FirstName = user.FirstName;
		this.LastName = user.LastName;
		this.City = user.City;
	}
}