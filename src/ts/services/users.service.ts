import {Injectable} from 'angular2/core';
import {User} from '../entities/user.entity';

@Injectable()
export class UsersService{
	private users: User[] = 
	[
		new User (1, "Jody", "Wilson", ""),
		new User (2, "Roman", "Gray", ""),
		new User (3, "Arlene", "Fuller", ""),
		new User (4, "Karl", "Gibbs", ""),
		new User (5, "Brett", "Kennedy", ""),
		new User (7, "Desiree", "Tyler", ""),
		new User (8, "Duane", "Luna", ""),
		new User (9, "Nelson", "Burton", ""),	
		new User (10, "Kerry", "Lynch", "")	
	];
	
	public getData () : User[]
	{
		return this.users;
	}	
	
	public getById (id: number) : User
	{
		return this.users.filter(u => u.Id == id)[0];
	}		
}