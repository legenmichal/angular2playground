import {Component, Input, OnInit} from 'angular2/core';
import {Router, RouteParams} from 'angular2/router';
import {UsersService} from '../services/users.service'; 
import {User} from '../entities/user.entity';
import {FORM_DIRECTIVES, CORE_DIRECTIVES} from 'angular2/common';

@Component({
    templateUrl: 'src/templates/user-detail.html',
    directives: [FORM_DIRECTIVES, CORE_DIRECTIVES]
})
export class UserDetailComponent implements OnInit { 
    private user: User; 
    private editedUser: User;
    
    constructor(
        private _service: UsersService, 
        private _routeParams: RouteParams, 
        private _router: Router)
    {
    }    
    
    ngOnInit() {
        this.user = this._service.getById(Number.parseInt(this._routeParams.get('id')));
        this.editedUser = User.Clone(this.user);
    }
    
    reset(){
        this.editedUser.Copy(this.user);
        return false;
    }
    
    cancel(){
        this._router.navigate(['UsersList', {}]);      
    }
    
    save(){
        this.user.FirstName = this.editedUser.FirstName;
        this.user.LastName = this.editedUser.LastName;
        this.user.City = this.editedUser.City;
        
        this._router.navigate(['UsersList', {}]);
    }
}