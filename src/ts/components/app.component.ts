import {Component} from 'angular2/core';
import {RouteConfig,  ROUTER_DIRECTIVES, ROUTER_PROVIDERS} from 'angular2/router';
import {UsersComponent} from './users-list.component';
import {AboutComponent} from './about.component';
import {UserDetailComponent} from './user-detail.component';
import {FormsComponent} from './forms.component';

@Component({
    selector: 'my-app',
    directives: [ ROUTER_DIRECTIVES],
    templateUrl: 'src/templates/app-template.html'
})
@RouteConfig([
    {path: '/', component: UsersComponent, name: 'UsersList', useAsDefault: true}, 
    {path: '/about', component: AboutComponent, name: 'About'  },
    {path: '/forms', component: FormsComponent, name: 'Forms'  },
    {path: '/user/:id', component: UserDetailComponent, name: 'UserDetail'  }
])
export class AppComponent { 
}