import {Component, OnInit} from 'angular2/core';
import {Router} from 'angular2/router';
import {UsersService} from '../services/users.service'; 
import {User} from '../entities/user.entity';
import {UserDetailComponent} from './user-detail.component';

@Component({
    templateUrl: 'src/templates/users-list.html', 	
    directives: [UserDetailComponent]
})
export class UsersComponent { 
    selectedUser: User;
    data: User[];
    
    constructor(
        private _service : UsersService, 
        private _router: Router)
    {
        this.data = this._service.getData();    
    }
    
    onSelect(item){
        this._router.navigate( ['UserDetail', { id: item.Id }] );  
    }        
}