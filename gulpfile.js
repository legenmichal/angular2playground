var gulp = require("gulp");
var ts = require("gulp-typescript");
var browserSync = require("browser-sync");
var tsProject = ts.createProject('tsconfig.json');
var del = require('del');
var runSequence = require('run-sequence');
var sass = require('gulp-sass');

gulp.task('dev-clean', function () {
	del('src/js/' + '**/*');
	del('src/css/' + '**/*');
});

gulp.task('dev-ts', function() {
	return tsProject.src()
	.pipe(ts(tsProject))
	.js
	.pipe(gulp.dest('src/js'));
});

gulp.task('reload', function() {
	browserSync.reload();
});

gulp.task('dev-sass', function() {
	return gulp.src('src/sass/*.scss')
	.pipe(sass())
	.pipe(gulp.dest('src/css/'));
});

gulp.task('dev-build', function(){
	runSequence(
		'dev-clean', ['dev-ts', 'dev-sass']
	);
})

gulp.task('dev', ['dev-build'], function() {
	browserSync({
		server:{
			baseDir: './',
			index: "./src/index.html"
		}
	})

    gulp.watch("src/ts/**/*.ts", ['dev-ts', 'reload']);
	gulp.watch("src/sass/*.scss", ['dev-sass','reload']);
});
